module Ejercicio3 where
	import Stack

	toList :: Stack a -> [a]
	toList s 
		| isEmpty s = []
		| otherwise = [top s] ++ toList (pop s)

	fromList :: [a] -> Stack a
	fromList [] = empty
	fromList (x:xs) = push x (fromList xs)