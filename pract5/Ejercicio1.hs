module Ejercicio1 where
	import qualified Circle
	import qualified Triangle

	main = do
	    let cir = (Circle.area 2)
	    putStrLn("Area de circulo con radio 2: " ++ show cir)
	    let tri = (Triangle.area 4 5)
	    putStrLn("Area de un triangulo con base 4 y altura 5: " ++ show tri)