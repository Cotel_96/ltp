module Repeated where

repeated :: Int -> [Int] -> Int
repeated x xs = length (filter (==x) xs)